package net.diamondmine.mcftchat.listeners;

import net.diamondmine.mcftchat.McftChat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * McftChat handler for all player related events.
 * 
 * @author Jon la Cour
 * @version 1.4.5
 */
public class Listeners implements Listener {

    private static McftChat plugin;

    /**
     * This just allows the main class to use us.
     * 
     * @param instance
     *            The main class
     */
    public Listeners(final McftChat instance) {
        plugin = instance;
    }

    /**
     * Pre-command processing.
     * 
     * @param event
     *            PlayerCommandPreprocessEvent
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public final void onPlayerCommandPreprocess(final PlayerCommandPreprocessEvent event) {
        String[] args = event.getMessage().split(" ", 2);
        if (args.length > 1) {
            String cmd = args[0];
            for (String channel : plugin.getSettings().keySet()) {
                if (cmd.equalsIgnoreCase("/" + channel)) {
                    event.setCancelled(true);
                    Player p = event.getPlayer();
                    if (p != null) {
                        p.chat("/mcftchat " + channel + " " + args[1]);
                    }
                    break;
                }
            }
        }

    }

    /**
     * Player chat tasks.
     * 
     * @param event
     *            PlayerChatEvent
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public final void onPlayerChat(final AsyncPlayerChatEvent event) {
        String name = event.getPlayer().getName();
        Player p = event.getPlayer();
        String message = event.getMessage();
        String channel = McftChat.getSpeakTo(name);
        if (!channel.equalsIgnoreCase("none")) {
            p.chat("/mcftchat " + channel + " " + message);
            event.setCancelled(true);
        }
    }
}
