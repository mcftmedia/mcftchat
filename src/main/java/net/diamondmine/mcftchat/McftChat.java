package net.diamondmine.mcftchat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.Logger;

import net.diamondmine.mcftchat.listeners.Listeners;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import ru.tehkode.permissions.bukkit.PermissionsEx;

/**
 * Core of McftChat.
 * 
 * @author Jon la Cour
 * @version 1.4.5
 */
public class McftChat extends JavaPlugin {
    public static final Logger logger = Logger.getLogger("Minecraft");
    private static Permission p = null;

    /**
     * Plugin disabled.
     */
    @Override
    public final void onDisable() {
        log("Version " + getDescription().getVersion() + " is disabled!");
    }

    /**
     * Plugin enabled.
     */
    @Override
    public final void onEnable() {
        // Permissions
        if (!setupPermissions()) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        checkConfig();
        loadConfig();

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new Listeners(this), this);

        PluginDescriptionFile pdfFile = getDescription();
        String version = pdfFile.getVersion();
        log("Version " + version + " enabled", "info");
    }

    @Override
    public final boolean onCommand(final CommandSender sender, final Command cmd, final String commandLabel, final String[] args) {
        String cmdname = cmd.getName().toLowerCase();
        Player player = null;
        String pname = ChatColor.DARK_RED + "[Console]";
        if (sender instanceof Player) {
            player = (Player) sender;
            pname = player.getName();
        }

        if (cmdname.equalsIgnoreCase("channels")) {
            CopyOnWriteArrayList<String> chans = new CopyOnWriteArrayList<String>();
            for (String s : getSettings().keySet()) {
                if (p.has(player, "mcftchat." + s + ".send")) {
                    chans.add(s);
                }
            }

            StringBuilder chanmsg = new StringBuilder("Channels: ");

            for (String s : chans) {
                chanmsg.append(s + " ");
            }

            player.sendMessage(chanmsg.toString());

        }

        if (cmdname.equalsIgnoreCase("joinchannel") && args.length == 1) {
            addUserToChannel(player, args[0]);
        }

        if (cmdname.equalsIgnoreCase("leavechannel") && args.length == 1) {
            removeUserFromChannel(player, args[0]);
        }

        if (cmdname.equalsIgnoreCase("speakto")) {
            if (args.length == 1) {
                changeSpeakTo(player, args[0]);
                return true;
            } else {
                String current = getSpeakTo(pname);
                sender.sendMessage("You are speaking to: " + current);
                return true;
            }

        }

        // for inputs like /d, /a, etc.
        String[] input = {""};
        for (String command : getSettings().keySet()) {
            if (cmdname.equalsIgnoreCase(command)) {
                input = args;
                input = unshift(cmdname, args);
                cmdname = "mcftchat";
            }
        }

        if (cmdname.equalsIgnoreCase("mcftchat") && input.length >= 2) {
            // arg 0 = channel name
            // arg 1+ = message
            String channelShortName = input[0];
            String message = "";
            for (Integer index = 1; index < input.length; index++) {
                message = message.concat(" " + input[index]);
            }
            if (getSettings().containsKey(channelShortName)) {
                if (player == null || p.has(player, "mcftchat." + channelShortName + ".send")) {
                    String channelLongName = getSettings().get(channelShortName);
                    boolean usetag = true;
                    String tag = "[" + tagconfig.get(channelLongName) + "] ";

                    if (tag.equals("[null] ") || tag.equals("[off] ")) {
                        usetag = false;
                    }

                    ChatColor color = ChatColor.valueOf(colorconfig.get(channelLongName));
                    String channeltag = color + tag;
                    String sendername = pname;

                    if (player != null) {
                        String worldname = player.getLocation().getWorld().getName();
                        String group = p.getPrimaryGroup(worldname, pname);
                        String prefix = groupPrefix(group, worldname);
                        String prefixcolor = prefix.replace("&", "");
                        if (prefixcolor.length() == 1) {
                            ChatColor usercolor = ChatColor.getByChar(prefixcolor);
                            sendername = usercolor + "[" + pname + "]";
                        }
                    }

                    Player[] players = getServer().getOnlinePlayers();
                    // Only send messages to channels a player has joined
                    if (!userInChannel(player, channelShortName)) {
                        addUserToChannel(player, channelShortName);
                    }
                    // only send message to players that have permission and are
                    // in the same channel
                    for (Player user : players) {
                        boolean haspermission = p.has(user, "mcftchat." + channelShortName + ".receive");
                        boolean inchannel = userInChannel(user, channelShortName);
                        if (haspermission && inchannel) {
                            if (usetag) {
                                user.sendMessage(channeltag + sendername + color + message);
                            } else {
                                user.sendMessage(sendername + color + ": " + message);
                            }
                        }

                    }

                    if (usetag) {
                        Bukkit.getServer().getConsoleSender().sendMessage(channeltag + sendername + color + message);
                    } else {
                        Bukkit.getServer().getConsoleSender().sendMessage(sendername + color + message);
                    }
                    return true;
                } else {
                    sender.sendMessage("Permission denied for '" + channelShortName + "'.");
                }
            }

        }
        return false;
    }

    private void changeSpeakTo(final Player user, final String channel) {
        boolean haspermission = p.has(user, "mcftchat." + channel + ".send");
        boolean channelexists = getSettings().containsKey(channel);
        if (haspermission && channelexists) {
            if (!userInChannel(user, channel)) {
                addUserToChannel(user, channel);
            }
            speakto.put(user.getName(), channel);
            user.sendMessage(ChatColor.GOLD + "Now speaking to " + getSettings().get(channel) + ".");
        } else if (channel.equalsIgnoreCase("none")) {
            speakto.remove(user.getName());
            user.sendMessage(ChatColor.GOLD + "Now speaking to main chat.");
        } else if (!channelexists) {
            user.sendMessage(ChatColor.GOLD + "Channel does not exist.");
        } else if (!haspermission) {
            user.sendMessage(ChatColor.GOLD + "You don't have permission to speak to " + getSettings().get(channel));
        }
    }

    public static String getSpeakTo(final String user) {
        if (speakto.containsKey(user)) {
            return speakto.get(user);
        } else {
            return "none";
        }
    }

    private boolean userInChannel(final Player user, final String channel) {
        String username = user.getName();
        if (channels.containsKey(username)) {
            return channels.get(username).contains(channel);
        } else {
            return false;
        }
    }

    /**
     * Attempts to add a user to a chat channel.
     * 
     * @param user
     *            User to be added
     * @param channel
     *            Channel to be added to
     * @return True if user was added to the channel, false otherwise.
     * @since 1.4.4
     */
    private boolean addUserToChannel(final Player user, final String channel) {
        String username = user.getName();
        boolean haspermission = p.has(user, "mcftchat." + channel + ".send");
        boolean channelexists = getSettings().containsKey(channel);
        if (haspermission && channelexists) { // if they have send permission,
            // they can join?
            if (channels.containsKey(username)) {
                if (!channels.get(username).add(channel)) {
                    return false;
                } else {
                    user.sendMessage(ChatColor.GOLD + "Joined " + getSettings().get(channel));
                    return true;
                }
            } else {
                // create an entry and place the user in it
                channels.put(username, new CopyOnWriteArraySet<String>());
                addUserToChannel(user, channel);
            }
        } else if (!channelexists) {
            user.sendMessage(ChatColor.GOLD + "Channel does not exist.");
        } else if (!haspermission) {
            user.sendMessage(ChatColor.GOLD + "You don't have permission to join " + getSettings().get(channel));
        }
        return false;
    }

    /**
     * Attempts to remove a user from a chat channel.
     * 
     * @param user
     *            User to be removed
     * @param channel
     *            Channel to be removed from
     * @return True if user was removed from the channel, false otherwise.
     * @since 1.4.4
     */
    private boolean removeUserFromChannel(final Player user, final String channel) {
        String username = user.getName();
        if (channels.containsKey(username)) {
            if (!channels.get(username).remove(channel)) {
                return false;
            } else {
                user.sendMessage("Left " + getSettings().get(channel));
                changeSpeakTo(user, "none");
                return true;
            }
        } else {
            // create an entry, no channels to remove
            channels.put(username, new CopyOnWriteArraySet<String>());
        }
        return false;
    }

    /**
     * Checks to make sure a permissions plugin has been loaded.
     * 
     * @return True if one is present, otherwise false.
     * @since 1.0.0
     */
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(
                net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            p = permissionProvider.getProvider();
        }
        return (p != null);
    }

    private final ConcurrentHashMap<String, String> settings = new ConcurrentHashMap<String, String>();
    private final ConcurrentHashMap<String, String> colorconfig = new ConcurrentHashMap<String, String>();
    private final ConcurrentHashMap<String, String> tagconfig = new ConcurrentHashMap<String, String>();
    private static HashMap<String, String> toggled = new HashMap<String, String>();
    // tracks who is speaking to what channel
    private static final ConcurrentHashMap<String, String> speakto = new ConcurrentHashMap<String, String>();
    // tracks participation in channels
    private static final ConcurrentHashMap<String, CopyOnWriteArraySet<String>> channels = new ConcurrentHashMap<String, CopyOnWriteArraySet<String>>();

    private final String baseDir = "plugins/McftChat";
    private final String configFile = "channels.txt";
    private final String tagFile = "tags.txt";
    private final String colorconfigFile = "colors.txt";

    /**
     * This makes sure that the settings directory exists and creates the
     * default settings file if one is not present.
     * 
     * @since 1.0.0
     */
    private void checkConfig() {
        // Creates base directory for config files
        File folder = new File(baseDir);
        if (!folder.exists()) {
            if (folder.mkdir()) {
                log("Created directory '" + baseDir + "'", "info");
            }
        }

        // Creates base config file
        String config = baseDir + "/" + configFile;
        File configfile = new File(config);
        if (!configfile.exists()) {
            BufferedWriter output;
            String newline = System.getProperty("line.separator");
            try {
                output = new BufferedWriter(new FileWriter(config));
                output.write("# Command = Channel" + newline);
                output.write("a = Admins" + newline);
                output.write("d = Donators" + newline);
                output.close();
                log("Created config file '" + config + "'", "info");
            } catch (Exception e) {
                log("Error creating configuration file '" + config + "'.", "warning");
            }
        }

        // Creates colors config file
        String colors = baseDir + "/" + colorconfigFile;
        File colorsfile = new File(colors);
        if (!colorsfile.exists()) {
            BufferedWriter output;
            String newline = System.getProperty("line.separator");
            try {
                output = new BufferedWriter(new FileWriter(colors));
                output.write("# Channel = CHAT_COLOR" + newline);
                output.write("# Available colors: http://jd.bukkit.org/apidocs/org/bukkit/ChatColor.html" + newline);
                output.write("Admins = LIGHT_PURPLE" + newline);
                output.write("Donators = DARK_AQUA" + newline);
                output.close();
                log("Created colors config file '" + colors + "'", "info");
            } catch (Exception e) {
                log("Error creating configuration file '" + colors + "'.", "warning");
            }
        }

        // Creates tag config file
        String tag = baseDir + "/" + tagFile;
        File tagfile = new File(tag);
        if (!tagfile.exists()) {
            BufferedWriter output;
            String newline = System.getProperty("line.separator");
            try {
                output = new BufferedWriter(new FileWriter(tag));
                output.write("# Channel = Tag" + newline);
                output.write("# This adds a tag before channel messages (i.e. [Admins] [laCour] Hello!)" + newline);
                output.write("# Put off if you do not want a channel tag for a channel. (i.e. Admins = off)" + newline);
                output.write("Admins = Staff" + newline);
                output.write("Donators = off" + newline);
                output.close();
                log("Created tag config file '" + tag + "'", "info");
            } catch (Exception e) {
                log("Error creating configuration file '" + tag + "'.", "warning");
            }
        }
    }

    /**
     * This loads all settings into a HashMap.
     * 
     * @since 1.0.0
     */
    private void loadConfig() {
        String config = baseDir + "/" + configFile;
        String colors = baseDir + "/" + colorconfigFile;
        String tag = baseDir + "/" + tagFile;
        String line = null;

        try {
            BufferedReader configuration = new BufferedReader(new FileReader(config));
            while ((line = configuration.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("#") && line.contains(" = ")) {
                    String[] pair = line.split(" = ", 2);
                    getSettings().put(pair[0], pair[1]);
                }
            }
            configuration.close();
            BufferedReader colorconfiguration = new BufferedReader(new FileReader(colors));
            while ((line = colorconfiguration.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("#") && line.contains(" = ")) {
                    String[] pair = line.split(" = ", 2);
                    colorconfig.put(pair[0], pair[1]);
                }
            }
            colorconfiguration.close();
            BufferedReader tagconfiguration = new BufferedReader(new FileReader(tag));
            while ((line = tagconfiguration.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("#") && line.contains(" = ")) {
                    String[] pair = line.split(" = ", 2);
                    tagconfig.put(pair[0], pair[1]);
                }
            }
            tagconfiguration.close();
        } catch (Exception e) {
            log("Unable to read configuration. We'll use the defaults for now.", "info");
        }
    }

    /**
     * @return Settings.
     */
    public final ConcurrentHashMap<String, String> getSettings() {
        return settings;
    }

    /**
     * @return Toggled map.
     */
    public static HashMap<String, String> getToggled() {
        return toggled;
    }

    /**
     * @param map
     *            Toggled map.
     */
    public static void setToggled(final HashMap<String, String> map) {
        McftChat.toggled = map;
    }

    /**
     * Unshifts the command arguments array.
     * 
     * @param str
     *            Command string.
     * @param array
     *            Arguments array.
     * @return Unshifted command arguments array.
     */
    private String[] unshift(final String str, final String[] array) {
        String[] newarray = new String[array.length + 1];
        newarray[0] = str;
        for (Integer i = 0; i < array.length; i++) {
            newarray[i + 1] = array[i];
        }
        return newarray;
    }

    /**
     * Returns a group's prefix string.
     * 
     * @param group
     *            Group name.
     * @param world
     *            World name.
     * @return Group prefix.
     */
    private String groupPrefix(final String group, final String world) {
        String prefix = PermissionsEx.getPermissionManager().getGroup(group).getPrefix();
        if (prefix == null) {
            return "";
        } else {
            return prefix;
        }
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send
     * @param type
     *            The level (info, warning, severe)
     * @since 1.4.0
     */
    public static void log(final String s, final String type) {
        String message = "[McftChat] " + s;
        String t = type.toLowerCase();
        if (t != null) {
            boolean info = t.equals("info");
            boolean warning = t.equals("warning");
            boolean severe = t.equals("severe");
            if (info) {
                logger.info(message);
            } else if (warning) {
                logger.warning(message);
            } else if (severe) {
                logger.severe(message);
            } else {
                logger.info(message);
            }
        }
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send.
     */
    public static void log(final String s) {
        log(s, "info");
    }
}
