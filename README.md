# McftChat

McftChat is a Bukkit server plugin for Minecraft that allows you to create chat channels for groups.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=CHAT)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/CHAT-CHAT)**
-- **[Download](http://diamondmine.net/plugins/download/McftChat)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/mcftchat/)**